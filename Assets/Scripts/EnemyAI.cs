using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;
using System.Collections;

public class EnemyAI : MonoBehaviour
{
    public NavMeshAgent agent;

    public GameObject player;

    public LayerMask whatIsGround, whatIsPlayer;

    public float health;
    public float angle;

    //Patroling
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;
    public int current;

    //Attacking
    public float timeBetweenAttacks;
    bool alreadyAttacked;
    bool isPatrolling;
    public GameObject projectile;

    //States
    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange;

    public Transform[] enemyPath;
    public FieldOfView fov;
    bool canSeePlayer;

    public MeshRenderer rendered;

    private void Awake()
    {
        player = GameObject.Find("Player");
        agent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        current = 0;
    }

    private void Update()
    {
        //Check for sight and attack range
        canSeePlayer = fov.canSeePlayer;
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer) && canSeePlayer;
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer) && canSeePlayer;
        

        if (!playerInSightRange && !playerInAttackRange) Patroling();
        if (playerInSightRange && !playerInAttackRange) ChasePlayer();
        if (playerInAttackRange && playerInSightRange) AttackPlayer();

        if (playerInSightRange)
        {
            rendered.material.color = Color.red;
        }
        else
        {
            rendered.material.color = Color.green;
        }
    }


    private void Patroling()
    {
        Vector3 distanceToWalkPoint = transform.position - enemyPath[current].transform.position;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            GoToNextPoint();
        else if (walkPointSet)
        {
            GoToNextPoint();
            walkPointSet = false;
        }
    }

    void GoToNextPoint()
    {
        if(current >= 2)
        {
            current = 0;
        }
        else
        {
            current++;
        }
        agent.destination = new Vector3(enemyPath[current].position.x, transform.position.y, enemyPath[current].position.z);
    }



    private void ChasePlayer()
    {
        agent.SetDestination(player.transform.position);
        walkPointSet = true;
    }

    private void AttackPlayer()
    {
        //Make sure enemy doesn't move
        agent.SetDestination(transform.position);

        transform.LookAt(player.transform);

        if (!alreadyAttacked)
        {
            ///Attack code here
            Rigidbody rb = Instantiate(projectile, transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
            rb.AddForce(transform.up * 8f, ForceMode.Impulse);
            ///End of attack code

            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0) Invoke(nameof(DestroyEnemy), 0.5f);
    }
    private void DestroyEnemy()
    {
        Destroy(gameObject);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }

}
