using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animationStateController : MonoBehaviour
{

    Animator animator;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        bool moveButtonPressed = Input.GetKey("w") || Input.GetKey("a") || Input.GetKey("d") || Input.GetKey("s");
        bool isRunning = Input.GetKey(KeyCode.LeftShift);
        if(moveButtonPressed)
        {
            animator.SetBool("IsWalking", true);
        }
        if (!moveButtonPressed)
        {
            animator.SetBool("IsWalking", false);
        }
        if (isRunning && moveButtonPressed)
        {
            animator.SetBool("IsWalking", false);
            animator.SetBool("IsRunning", true);
            
        }
        if (!isRunning && moveButtonPressed)
        {
            animator.SetBool("IsWalking", true);
            animator.SetBool("IsRunning", false);
        }
        if(isRunning && !moveButtonPressed)
        {
            animator.SetBool("IsRunning", false);
            animator.SetBool("IsWalking", false);
        }
    }
}
